"""Scientific Computation Project 3, part 2
CID: 01333257
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy import sparse
import time

def microbes(phi,kappa,mu,L = 1024,Nx=1024,Nt=1201,T=600,display=False):
    """
    Question 2.2
    Simulate microbe competition model

    Input:
    phi,kappa,mu: model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of f when true

    Output:
    f,g: Nt x Nx arrays containing solution
    """

    #generate grid
    L = 1024
    x = np.linspace(0,L,Nx)
    dx = x[1]-x[0]
    dx2inv = 1/dx**2

    def RHS(y,t,k,r,phi,dx2inv):
        #RHS of model equations used by odeint

        n = y.size//2

        f = y[:n]
        g = y[n:]

        #Compute 2nd derivatives
        d2f = (f[2:]-2*f[1:-1]+f[:-2])*dx2inv
        d2g = (g[2:]-2*g[1:-1]+g[:-2])*dx2inv

        #Construct RHS
        R = f/(f+phi)
        dfdt = d2f + f[1:-1]*(1-f[1:-1])- R[1:-1]*g[1:-1]
        dgdt = d2g - r*k*g[1:-1] + k*R[1:-1]*g[1:-1]
        dy = np.zeros(2*n)
        dy[1:n-1] = dfdt
        dy[n+1:-1] = dgdt

        #Enforce boundary conditions
        a1,a2 = -4/3,-1/3
        dy[0] = a1*dy[1]+a2*dy[2]
        dy[n-1] = a1*dy[n-2]+a2*dy[n-3]
        dy[n] = a1*dy[n+1]+a2*dy[n+2]
        dy[-1] = a1*dy[-2]+a2*dy[-3]

        return dy


    #Steady states
    rho = mu/kappa
    F = rho*phi/(1-rho)
    G = (1-F)*(F+phi)
    y0 = np.zeros(2*Nx) #initialize signal
    y0[:Nx] = F
    y0[Nx:] = G + 0.01*np.cos(10*np.pi/L*x) + 0.01*np.cos(20*np.pi/L*x)

    t = np.linspace(0,T,Nt)

    #compute solution
    print("running simulation...")
    y = odeint(RHS,y0,t,args=(kappa,rho,phi,dx2inv),rtol=1e-6,atol=1e-6)
    f = y[:,:Nx]
    g = y[:,Nx:]
    print("finished simulation")
    if display:
        plt.figure()
        plt.contour(x,t,f)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of f')


    return f,g


def newdiff(f,h):
    """
    Question 2.1 i)
    Input:
        f: array whose 2nd derivative will be computed
        h: grid spacing
    Output:
        d2f: second derivative of f computed with compact fd scheme
    """

    
    N = len(f)

    # Coefficients for compact fd scheme
    alpha = 9/38
    a = (696-1191*alpha)/428
    b = (2454*alpha-294)/535
    c = (1179*alpha-344)/2140
    
    # Compute M 
    I = np.identity(N)
    zeros_list1 = np.zeros(N-1)
    zeros_list2 = np.zeros([N,1])
    alpha_diag = np.diag(zeros_list1 + alpha)
    alpha_d = np.hstack([np.vstack([zeros_list1,alpha_diag]),zeros_list2])
    M = I + alpha_d + alpha_d.T
    M[0,1],M[N-1,N-2] = 10,10
    M = sparse.csr_matrix(M)
    
    # Initialize x
    x = np.zeros(N)
    
    # Compute first and last value of x
    x[0] = 145*f[0]/12 - 76*f[1]/3 + 29*f[2]/2 - 4*f[3]/3 + f[4]/12
    x[N-1] = 145*f[N-1]/12 - 76*f[N-2]/3 + 29*f[N-3]/2 - 4*f[N-4]/3 + f[N-5]/12
    
    # Compute z
    z = np.array([c/9, b/4, a, -2*(c/9+b/4+a), a, b/4, c/9])
    # Add 2 entries at start and end of f
    f = np.hstack([f[N-3:N-1],f,f[1:3]])
    
    # Compute the rest of x
    for i in range(3,N+1):
        x[i-2] = np.dot(z, f[i-3:i+4])
    
    # Divide all elements of x by h squared
    x = x/(h**2)
    
    # Compute second derivative
    d2f = sparse.linalg.spsolve(M,x)

    return d2f #modify as needed


# Define fuction to compute second derivative using centered scheme
def centereddiff(f,h):
    
    N = len(f)
    
    #Compute M
    I = np.identity(N-2)
    zeros = np.zeros([N-2,2])
    zeros2 = np.zeros([N-2,1])
    M = np.hstack([I,zeros]) + np.hstack([zeros,I]) + np.hstack([zeros2,-2*I,zeros2])
    M = sparse.csr_matrix(M)
    
    # Compute second derivative for i = 1,...,N-2
    df2 = M.dot(f)
    # Compute second derivative for i = 0 and i = N-1 and add these to array
    df2 = np.hstack([f[1]-2*f[0]+f[-2],df2,f[-2]-2*f[-1]+f[1]])
    # Divide through by h squared
    df2 = df2/(h**2)
    
    return df2
    
    

def analyzefd():
    """
    Question 2.1 ii)
    Add input/output as needed
    
    """
    #Comment and uncomment as necessary
    
    
    titles = ['Periodic exp(x)', 'Average Error', 'Total Error', 'Time']
    fig, axs = plt.subplots(2, 2)
    fig.tight_layout(pad=1)
    fig.set_size_inches(12, 8)
    plt.subplots_adjust(hspace = 0.3)
    
    x = np.linspace(0, 3, 200)
    f = np.exp(x)
    x = np.hstack([x-3,x,x+3])
    f = np.hstack([f,f,f])
    #x = np.linspace(-np.pi,3*np.pi,200)
    #f = np.sin(x)
    axs[1,0].plot(x, f, color = 'b')
    axs[1,0].set_title(titles[0])
    axs[1,0].set(xlabel="x", ylabel="f(x)")
    
    N = np.array(list(range(1,101)))*10
    #N = N[14:]
    N = N[10:]
    mean_error = []
    error = []
    times = []
    for i,j in enumerate(N):
        x = np.linspace(0, 3, j)
        #x = np.linspace(0,2*np.pi,j)
        x[-1] = x[0]
        h = x[1]-x[0]
        #f = np.sin(x)
        f = np.exp(x)
        t1 = time.time()
        #df2 = newdiff(f,h)
        df2 = centereddiff(f,h)
        t2 = time.time()
        times.append(t2-t1)
        #df2_exact = -np.sin(x)
        df2_exact = np.exp(x)
        diff = df2 - df2_exact
        diff = diff[10:91]
        diff = abs(diff)
        
        error.append(sum(diff))
        mean_error.append(np.mean(diff))
    
    axs[0,0].plot(N, mean_error, color = 'green')
    axs[0,0].set_title(titles[1])
    axs[0,0].set(xlabel="N", ylabel="Mean Error")
    axs[0,1].plot(N, error, color = 'red')
    axs[0,1].set_title(titles[2])
    axs[0,1].set(xlabel="N", ylabel="Total Error")
    axs[1,1].plot(N, times, color = 'rebeccapurple')
    axs[1,1].set_title(titles[3])
    axs[1,1].set(xlabel="N", ylabel="Time")
    
    
    
    
    """
    #Wavenumber analysis 1
    #===================================================================
        
    # Coefficients for compact fd scheme
    alpha = 9/38
    a = (696-1191*alpha)/428
    b = (2454*alpha-294)/535
    c = (1179*alpha-344)/2140
    kh = np.linspace(0,4,200)
    
    modified_wavenumber = 2*(c*(np.cos(3*kh)-1)/9 + b*(np.cos(2*kh)-1)/4 + 
                            a*(np.cos(kh)-1))
    
    exact = -(kh**2)*(2*alpha*np.cos(kh)+1)
    
    plt.plot(kh, exact, color = 'lawngreen')
    plt.plot(kh, modified_wavenumber, color = 'b')
    plt.legend(['Exact', 'Modified Wavenumber'])
    plt.xlabel("kh")
    
    percent_error = 100 * abs((modified_wavenumber[1:]-exact[1:])/exact[1:])
    for i,j in enumerate(percent_error):
        if j >= 1:
            print(i)
            break
            
    """      
    """
    kh = np.linspace(0,4,200)
    
    modified_wavenumber = 145/12 - 76*np.exp(-1j*kh)/3 + 29*np.exp(-2j*kh)/2 - 4*np.exp(-3j*kh)/3 + np.exp(-4j*kh)/12
    
    exact = -(kh**2)*(1 + 10*np.exp(-1j*kh))
    
    plt.plot(kh, abs(exact), color = 'lawngreen')
    plt.plot(kh, abs(modified_wavenumber), color = 'b')
    plt.legend(['Exact', 'Modified Wavenumber'])
    plt.xlabel("kh")
    
    percent_error = 100 * abs((modified_wavenumber[1:]-exact[1:])/exact[1:])
    for i,j in enumerate(percent_error):
        if j >= 1:
            print(i)
            break
    
    #===================================================================
    """
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    """
    #Wavenumber analysis 2
    #===================================================================
    
    kh = np.linspace(0,4,200)
    
    modified_wavenumber = 2*(np.cos(kh)-1)
    
    exact = -(kh**2)
    
    plt.plot(kh, exact, color = 'lawngreen')
    plt.plot(kh, modified_wavenumber, color = 'b')
    plt.legend(['Exact', 'Modified Wavenumber'])
    plt.xlabel("kh")
    
    percent_error = 100 * abs((modified_wavenumber[1:]-exact[1:])/exact[1:])
    for i,j in enumerate(percent_error):
        if j >= 1:
            print(i)
            break
    
    #===================================================================
    
    
    return None #modify as needed
    """

def dynamics():
    """
    Question 2.2
    Add input/output as needed

    """

    return None #modify as needed

if __name__=='__main__':
    x=None
    #Add code here to call functions above and
    #generate figures you are submitting
