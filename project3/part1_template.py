"""Scientific Computation Project 3, part 1
CID: 01333257
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def hfield(r,th,h,levels=50):
    """Displays height field stored in 2D array, h,
    using polar grid data stored in 1D arrays r and th.
    Modify as needed.
    """
    thg,rg = np.meshgrid(th,r)
    xg = rg*np.cos(thg)
    yg = rg*np.sin(thg)
    plt.figure()
    plt.contourf(xg,yg,h,levels)
    plt.axis('equal')
    return None

def repair1(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    #problem setup
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    S = set()
    for i,j in zip(iK,jK):
        S.add((i,j))

    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))

    #Create lists of indices used during optimization
    mlist = [[] for i in range(a)]
    nlist = [[] for j in range(b)]

    for i,j in zip(iK,jK):
        mlist[i].append(j)
        nlist[j].append(i)

    dA = np.zeros(niter)
    dB = np.zeros(niter)

    for z in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    Bfac = 0.0
                    Asum = 0

                    for j in mlist[m]:
                        Bfac += B[n,j]**2
                        Rsum = 0
                        for k in range(p):
                            if k != n: Rsum += A[m,k]*B[k,j]
                        Asum += (R[m,j] - Rsum)*B[n,j]

                    A[m,n] = Asum/(Bfac+l) #New A[m,n]
                if m < p:
                    #Add code here to update B[m,n]
                    B[m,n]=None #modify
        dA[z] = np.sum(np.abs(A-Aold))
        dB[z] = np.sum(np.abs(B-Bold))
        if z%10==0: print("z,dA,dB=",z,dA[z],dB[z])


    return A,B


def repair2(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R. Efficient and complete version of repair1.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    #problem setup
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))

    #Add code here
    
    R1 = np.matmul(A,B)
    R1[aK,bK] = -1000
    Rdiff = R0 - R1
    
    aKA,bKA,aKB,bKB = [],[],[],[]
    for i,j in zip(aK,bK):
        if j < p:
            aKA.append(i)
            bKA.append(j)
        if i < p:
            aKB.append(i)
            bKB.append(j)
            
    A0 = A.copy()
    B0 = B.copy()
    A0[aKA,bKA] = 0 
    B0[aKB,bKB] = 0
    
    Afac = (A0**2).sum(axis = 0)
    Bfac = (B0**2).sum(axis = 1)
    
    
    #Create lists of indices used during optimization
    mlist = [[] for i in range(a)]
    nlist = [[] for j in range(b)]

    for i,j in zip(iK,jK):
        mlist[i].append(j)
        nlist[j].append(i)
    """
    mdict = {}
    ndict = {}
    for i in range(a):
        mdict[i] = []
    for j in range(b):
        ndict[j] = []
    
    for i,j in zip(iK,jK):
        mdict[i].append(j)
        ndict[j].append(i)
    """
    

    dA = np.zeros(niter)
    dB = np.zeros(niter)

    for z in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    Amn = A[m,n]
                    
                    Asum = np.dot(Rdiff[m],B[n])

                    A[m,n] = (Asum+Amn*Bfac[n])/(Bfac[n]+l) #New A[m,n]
                    
                    for j in mlist[m]:
                        Rdiff[m,j] += -(A[m,n]-Amn)*B[n,j]
                    if A0[m,n] != 0:
                        Afac[n] += A[m,n]**2 - Amn**2
                    
                if m < p:
                    #Add code here to update B[m,n]
                    Bmn = B[m,n]
                    
                    Bsum = np.dot(Rdiff[:,n],A[:,m])

                    B[m,n] = (Bsum+Bmn*Afac[m])/(Afac[m]+l) #New B[m,n]
                    
                    for i in nlist[n]:
                        Rdiff[i,n] += -A[i,m]*(B[m,n]-Bmn)
                    if B0[m,n] != 0:
                        Bfac[m] += B[m,n]**2 - Bmn**2
                    
        dA[z] = np.sum(np.abs(A-Aold))
        dB[z] = np.sum(np.abs(B-Bold))
        if z%9==0: print("z,dA,dB=",z,dA[z],dB[z])
    
    return A,B




def repair3(R,p,l=1.0,niter=10,inputs=()):
    """
    Question 1.1: Repair corrupted data stored in input
    array, R.
    Input:
        R: 2-D data array (should be loaded from data1.npy)
        p: dimension parameter
        l: l2-regularization parameter
        niter: maximum number of iterations during optimization
        inputs: can be used to provide other input as needed
    Output:
        A,B: a x p and p x b numpy arrays set during optimization
    """
    #problem setup
    R0 = R.copy()
    a,b = R.shape
    iK,jK = np.where(R0 != -1000) #indices for valid data
    aK,bK = np.where(R0 == -1000) #indices for missing data

    S = set()
    for i,j in zip(iK,jK):
        S.add((i,j))

    #Set initial A,B
    A = np.ones((a,p))
    B = np.ones((p,b))

    #Create lists of indices used during optimization
    mlist = [[] for i in range(a)]
    nlist = [[] for j in range(b)]

    for i,j in zip(iK,jK):
        mlist[i].append(j)
        nlist[j].append(i)

    dA = np.zeros(niter)
    dB = np.zeros(niter)

    for z in range(niter):
        Aold = A.copy()
        Bold = B.copy()

        #Loop through elements of A and B in different
        #order each optimization step
        for m in np.random.permutation(a):
            for n in np.random.permutation(b):
                if n < p: #Update A[m,n]
                    Bfac = 0.0
                    Asum = 0

                    for j in mlist[m]:
                        Bfac += B[n,j]**2
                        Rsum = 0
                        for k in range(p):
                            if k != n: Rsum += A[m,k]*B[k,j]
                        Asum += (R0[m,j] - Rsum)*B[n,j]

                    A[m,n] = Asum/(Bfac+l) #New A[m,n]
                if m < p:
                    #Add code here to update B[m,n]
                    Afac = 0.0
                    Bsum = 0

                    for i in nlist[n]:
                        Afac += A[i,m]**2
                        Rsum = 0
                        for k in range(p):
                            if k != m: Rsum += A[i,k]*B[k,n]
                        Bsum += (R0[i,n] - Rsum)*A[i,m]

                    B[m,n] = Bsum/(Afac+l) #New B[m,n]
                    
        dA[z] = np.sum(np.abs(A-Aold))
        dB[z] = np.sum(np.abs(B-Bold))
        if z%1==0: print("z,dA,dB=",z,dA[z],dB[z])

    return A,B


    

def outwave(r0):
    """
    Question 1.2i)
    Calculate outgoing wave solution at r=r0
    See code/comments below for futher details
        Input: r0, location at which to compute solution
        Output: B, wave equation solution at r=r0

    """
    A = np.load('data2.npy')
    r = np.load('r.npy')
    th = np.load('theta.npy')

    Nr,Ntheta,Nt = A.shape
    B = np.zeros((Ntheta,Nt))

    return B

def analyze1():
    """
    Question 1.2ii)
    Add input/output as needed

    """

    return None #modify as needed




def reduce(H,inputs=()):
    """
    Question 1.3: Construct one or more arrays from H
    that can be used by reconstruct
    Input:
        H: 3-D data array
        inputs: can be used to provide other input as needed
    Output:
        arrays: a tuple containing the arrays produced from H
    """

    #Add code here

    arrays = () #modify as needed
    return arrays


def reconstruct(arrays,inputs=()):
    """
    Question 1.3: Generate matrix with same shape as H (see reduce above)
    that has some meaningful correspondence to H
    Input:
        arrays: tuple generated by reduce
        inputs: can be used to provide other input as needed
    Output:
        Hnew: a numpy array with the same shape as H
    """

    #Add code here

    Hnew = None #modify

    return Hnew


if __name__=='__main__':
    x=None
    #Add code here to call functions above and
    #generate figures you are submitting
