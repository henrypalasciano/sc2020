"""Scientific Computation Project 2, part 2
Your CID here: 01333257
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import matplotlib.pyplot as plt



def rwgraph(G,i0=0,M=100,Nt=100):
    """ Question 2.1
    Simulate M Nt-step random walks on input graph, G, with all
    walkers starting at node i0
    Input:
        G: An undirected, unweighted NetworkX graph
        i0: intial node for all walks
        M: Number of walks
        Nt: Number of steps per walk
    Output: X: M x Nt+1 array containing the simulated trajectories
    """
    X = np.zeros((M,Nt+1))

    for i in range(M):
        X[i,0] = i0
        n = i0
        for j in range(1,Nt+1):
            n = np.random.choice(list(G.neighbors(n)))
            X[i,j] = n

    return X


def rwgraph_analyze1(input=(None)):
    """Analyze simulated random walks on
    Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    
    #Comment and uncomment code as necessary
    
    ba = nx.barabasi_albert_graph(2000,4,121)
    node_degrees = list(dict(ba.degree()).values())
    
    """
    start = 0
    degree = 0
    D1 = {}
    # Find starting node
    for i in range(2000):
        D1[i] = 0
        if node_degrees[i] > degree:
            start = i
            degree = node_degrees[i]
    
    print("Starting node = ", start)
    print("Starting node degree = ", node_degrees[start])
    
    Nt = [500, 5000, 50000]
    M = 100 #1, 1000
    l = [0, 1, 2]
    titles = ["Nt = {}".format(i) for i in Nt]
    fig, axs = plt.subplots(3, 2)
    fig.tight_layout(pad=1)
    fig.set_size_inches(12, 12)
    plt.subplots_adjust(hspace = 0.3)
    for j,i in enumerate(Nt):
        X = rwgraph(ba, start, M, i)
        D = D1.copy()
        for k in range(M):
            for m in X[k,]:
                D[m] = D[m] + 1
        
        # Average over number of simulations
        for n in range(len(D)):
            D[n] = D[n]/M
        
        D_vals = list(D.values())
          
        axs[j,0].scatter(list(range(2000)), list(D_vals),color='b')
        axs[j,1].scatter(node_degrees, list(D_vals),color='b')
        axs[j,0].set_title(titles[j])
        axs[j,1].set_title(titles[j])
        axs[j,0].set(xlabel="Node Number", ylabel="Average Number of Visits")
        axs[j,1].set(xlabel="Node Degree", ylabel="Average Number of Visits")
        
    """
    
    # Compute Q as defined in (iii) and its inverse
    Q = np.diag(node_degrees)
    Q_inv = np.linalg.inv(Q)
    # Compute A
    A = nx.adj_matrix(ba).todense()
    # Compute T and its transpose
    T = np.matmul(Q_inv,A)
    T_t = np.transpose(T)
    # Compute the eigenvalues and eigenvectors of T_t
    e, P = np.linalg.eig(T_t)
    for i,j in enumerate(e):
        if j<1.01 and j>0.99:
            pos = i
    # Compute the stationary distribution
    evec = np.transpose(P)[pos]
    p = abs(evec).tolist()[0]
    m = sum(p)
    pi = [i/m for i in p]
    
    # Scatter plot of node degree against stationary distribution
    plt.scatter(node_degrees, pi, color = 'b')
    plt.ylim(-0.001,0.011)
    plt.xlabel("Node Degree")
    plt.ylabel("Stationary Distribution")
        
    return None #modify as needed


def rwgraph_analyze2(input=(None)):
    """Analyze similarities and differences
    between simulated random walks and linear diffusion on
    Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    #Comment and uncomment code as necessary
    
    ba = nx.barabasi_albert_graph(2000,4,121)
    node_degrees = list(dict(ba.degree()).values())

    # Compute Q and Q_inv
    Q = np.diag(node_degrees)
    Q_inv = np.linalg.inv(Q)
    A = nx.adj_matrix(ba).todense()
    # Compute L
    L = Q - A
    e, P = np.linalg.eig(L)
    
    """# Find position of min(e)
    for i,j in enumerate(e):
        if j == min(e):
            print("smallest eigenvalue of L at i = ",i)
    # Therefore it is the i+1 th eigenvalue, since python starts from 0
    # Check that the second smallest eigenvalue cannot be considered equal to 0
    print("second smallest eigenvalue of L = ",min(e.tolist()[:136]+e.tolist()[137:]))
    """
    
    # Scaled Laplacian
    # LS = I - Q_inv * A = Q_inv * L
    LS = np.matmul(Q_inv,L)
    eS, PS = np.linalg.eig(LS)
    """for i,j in enumerate(eS):
        if j == min(eS):
            print("smallest eigenvalue of LS at i = ",i)
    # Therefore it is the i+1 th eigenvalue, since python starts from 0
    # Check that the second smallest eigenvalue cannot be considered equal to 0
    print("second smallest eigenvalue of LS = ",min(eS.tolist()[1:]))
    """
    
    # Scaled Laplacian transpose
    LS_t = np.transpose(LS)
    eS_t, PS_t = np.linalg.eig(LS_t)
    
    Nt = 5 #1, 100, 1000
    M = 10000 
    
    titles = ["Random Walk","Laplacian","Scaled Laplacian","Scaled Laplacian Transpose"]
    fig, axs = plt.subplots(4, 2)
    fig.tight_layout(pad=1)
    fig.set_size_inches(12, 16)
    plt.subplots_adjust(hspace = 0.3)
    
    # Number of visits to last node
    X = rwgraph(ba, 5, M, Nt)
    
    D={}
    for i in range(2000):
        D[i] = 0
    for k in X[:,Nt]:
        D[k] = D[k] + 1
    for j in range(2000):
        D[j] = D[j]/M
        
    D_vals = list(D.values())
    
    # Initial conditions
    f0 = np.zeros(2000)
    f0[5] = 1
    
    # Compute constants
    P_inv = np.linalg.inv(P)
    c = np.array(np.dot(P_inv,f0).tolist()[0])
    PS_inv = np.linalg.inv(PS)
    k = np.array(np.dot(PS_inv,f0).tolist()[0])
    PS_t_inv = np.linalg.inv(PS_t)
    h = np.array(np.dot(PS_t_inv,f0).tolist()[0])
    
    # Solution for Laplacian
    e_exp = np.exp(-e*Nt)
    ft = np.dot(P, c*e_exp).tolist()[0]
    
    # Solution for scaled Laplacian
    eS_exp = np.exp(-eS*Nt)
    ftS = np.dot(PS, k*eS_exp).tolist()[0]
    
    # Solution for scaled Laplacian transpose
    eS_t_exp = np.exp(-eS_t*Nt)
    ftS_t = np.dot(PS_t, k*eS_t_exp).tolist()[0]
    
    
    axs[0,0].scatter(list(range(2000)), D_vals, color='b')
    axs[0,0].set_title(titles[0])
    axs[0,0].set(ylim = (-0.002,0.014))
    axs[0,1].scatter(list(range(2000)), ft, color='b')
    axs[0,1].set_title(titles[1])
    axs[1,0].scatter(list(range(2000)), ftS, color='b')
    axs[1,0].set_title(titles[2])
    axs[1,1].scatter(list(range(2000)), ftS_t, color='b')
    axs[1,1].set_title(titles[3])
    axs[2,0].scatter(node_degrees, D_vals, color='b')
    axs[2,0].set_title(titles[0])
    axs[2,0].set(ylim = (-0.002,0.014))
    axs[2,1].scatter(node_degrees, ft, color='b')
    axs[2,1].set_title(titles[1])
    axs[3,0].scatter(node_degrees, ftS, color='b')
    axs[3,0].set_title(titles[2])
    axs[3,1].scatter(node_degrees, ftS_t, color='b')
    axs[3,1].set_title(titles[3])
    
    
    
    axs[0,0].set(xlabel="Node Number", ylabel="Average Number of Visits")
    
    for ax in axs.flat[1:4]:
        ax.set(xlabel="Node Number", ylabel="Flux")
    
    axs[2,0].set(xlabel="Node Degree", ylabel="Average Number of Visits")
    
    for ax in axs.flat[5:]:
        ax.set(xlabel="Node Degree", ylabel="Flux")




    """
    
    f0L = -100 * P[:,136]
    # For LS and LS_t zero eigenvector in same position (0)
    f0LS = -100 * PS[:,0]
    # Some entries in eigenvector corresponding to zero eigenvalue are much larger 
    # than those in the corresponding eigenvectors of L and LS, therefore only take
    # a multiple of -10
    f0LS_t = -10 * PS_t[:,0]
    
    e_exp1 = np.exp(-e*100)
    c1 = np.array([i[0] for i in np.dot(P_inv,f0L).tolist()])
    ft1 = np.dot(P, c1*e_exp1).tolist()[0]
    
    eS_exp1 = np.exp(-eS*100)
    k1 = np.array([i[0] for i in np.dot(PS_inv,f0LS).tolist()])
    ftS1 = np.dot(PS, k1*eS_exp1).tolist()[0]
    
    eS_t_exp1 = np.exp(-eS_t*100)
    h1 = np.array([i[0] for i in np.dot(PS_t_inv,f0LS_t).tolist()])
    ftS_t1 = np.dot(PS_t, h1*eS_t_exp1).tolist()[0]
    
    titles = ["Random Walk","Laplacian","Scaled Laplacian","Scaled Laplacian Transpose"]
    fig, axs = plt.subplots(3, 2)
    fig.tight_layout(pad=1)
    fig.set_size_inches(12, 12)
    plt.subplots_adjust(hspace = 0.3)
    
    axs[0,0].scatter(node_degrees, [i[0] for i in f0L.tolist()], color='r')
    axs[0,0].set_title(titles[1])
    axs[1,0].scatter(node_degrees, [i[0] for i in f0LS.tolist()], color='r')
    axs[1,0].set_title(titles[2])
    axs[2,0].scatter(node_degrees, [i[0] for i in f0LS_t.tolist()], color='r')
    axs[2,0].set_title(titles[3])
    
    axs[0,1].scatter(node_degrees, ft1, color='b')
    axs[0,1].set_title(titles[1])
    axs[1,1].scatter(node_degrees, ftS1, color='b')
    axs[1,1].set_title(titles[2])
    axs[2,1].scatter(node_degrees, ftS_t1, color='b')
    axs[2,1].set_title(titles[3])
    
    
    for ax in axs.flat:
        ax.set(xlabel="Node Degree", ylabel="Flux")
        
    """
    
    
    
    return None #modify as needed



def modelA(G,x=0,i0=0.1,beta=1.0,gamma=1.0,tf=5,Nt=1000):
    """
    Question 2.2
    Simulate model A

    Input:
    G: Networkx graph
    x: node which is initially infected with i_x=i0
    i0: magnitude of initial condition
    beta,gamma: model parameters
    tf,Nt: Solutions are computed at Nt time steps from t=0 to t=tf (see code below)

    Output:
    iarray: N x Nt+1 Array containing i across network nodes at
                each time step.
    """

    N = G.number_of_nodes()
    iarray = np.zeros((N,Nt+1))
    tarray = np.linspace(0,tf,Nt+1)
    
    # Adjacency matrix
    A = nx.adj_matrix(G).todense()
    ones = np.ones(N)
    # Initial y
    y = np.zeros(N)
    y[x] = i0
    iarray[:,0] = y
    # Time step
    t = tf/Nt
    neg_beta = -beta
    gamma_adj = gamma * A

    def RHS(y,t):
        """Compute RHS of modelA at time t
        input: y should be a size N array
        output: dy, also a size N array corresponding to dy/dt

        Discussion: add discussion here
        """
        # Compute dy
        dy = neg_beta * y + np.asarray((np.dot(gamma_adj, y)))[0] * (ones - y)
        
        return dy
    
    # For loop computing y at each time point and appending to iarray
    for j in range(1,Nt+1):
        dy = RHS(y,tarray[j])
        y = y + t * dy
        iarray[:,j] = y
    
    return iarray


def transport(input=(None)):
    """Analyze transport processes (model A, model B, linear diffusion)
    on Barabasi-Albert graphs.
    Modify input and output as needed.
    """
    
    ba = nx.barabasi_albert_graph(100, 5, 121)
    node_degrees = list(dict(ba.degree()).values())
    
    """
    m = max(node_degrees)
    for j,k in enumerate(node_degrees):
        if k == m:
            print(j,k)
            break
    """

    # Compute Q and Q_inv
    Q = np.diag(node_degrees)
    Q_inv = np.linalg.inv(Q)
    A = nx.adj_matrix(ba).todense()
    # Compute L
    L = Q - A
    I = np.identity(100)
    Z = np.zeros((100,100))
    alpha = - 0.1
    M = np.vstack((np.hstack((Z,alpha*L)),np.hstack((I,Z))))
    
    e, P = np.linalg.eig(M)
    
    i_s = np.zeros(200)
    i_s[5] = 0.1
    
    P_inv = np.linalg.inv(P)
    c = np.array(np.dot(P_inv,i_s)).tolist()[0]
    
    """
    t = [1, 5, 100, 10000, 1000000]
    
    titles = ["t = 1","t = 5","t = 100","t = 10000","t = 1000000"]
    fig, axs = plt.subplots(5, 2)
    fig.tight_layout(pad=1)
    fig.set_size_inches(12, 20)
    plt.subplots_adjust(hspace = 0.3)
    
    l = list(range(100))
    
    for j,k in enumerate(t):
        e_exp = np.exp(e*k)
        sol = np.dot(P, c*e_exp).tolist()[0]
        i = sol[:100]
        s = sol[100:]
        
        axs[j,0].scatter(l, i, color='b')
        axs[j,0].set_title(titles[j])
        axs[j,1].scatter(l, s, color='g')
        axs[j,1].set_title(titles[j])
        
    for ax in axs.flat:
        ax.set(xlabel="Node Number", ylabel="Flux")
        
    """    
    """
    e_exp = np.exp(e*t)
    sol = np.dot(P, c*e_exp).tolist()[0]
    i = sol[:100]
    s = sol[100:]
    
    ireal = [k.real for k in i]
    iimag = [k.imag for k in i]
    plt.scatter(ireal,iimag)
    plt.ylim((min(iimag)*1.1,max(iimag)*1.1))
    plt.xlim((min(ireal)*1.1,max(ireal)*1.3))
    """
    t = [1,2,3,4,5]
    tarray = np.linspace(0,5,1001)
    
    l2 = list(range(100))
    
    colors = ['b','g','r','y','o']
    
    irlist = [[],[],[],[],[]]
    iilist = [[],[],[],[],[]]
    nodes = [0,10,40,70,99]
    
    for j in tarray:
        e_exp = np.exp(e*j)    
        sol = np.dot(P, c*e_exp).tolist()[0]
        i = sol[:100]
        s = sol[100:]
        for n in range(5):
            irlist[n].append(i[nodes[n]].real)
            iilist[n].append(i[nodes[n]].imag)
            
    for m in range(5):
       plt.plot(irlist[m],iilist[m],colors[m])
    

    return irlist,iilist #None #modify as needed







if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    G=None #modify as needed
