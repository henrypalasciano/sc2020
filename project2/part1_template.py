"""Scientific Computation Project 2, part 1
Your CID here: 01333257
"""

import collections as col

def flightLegs(Alist,start,dest):
    """
    Question 1.1
    Find the minimum number of flights required to travel between start and dest,
    and  determine the number of distinct routes between start and dest which
    require this minimum number of flights.
    Input:
        Alist: Adjacency list for airport network
        start, dest: starting and destination airports for journey.
        Airports are numbered from 0 to N-1 (inclusive) where N = len(Alist)
    Output:
        Flights: 2-element list containing:
        [the min number of flights between start and dest, the number of distinct
        jouneys with this min number]
        Return an empty list if no journey exist which connect start and dest
    """
    
    # If start and dest are the same, then there is 1 shortest path of length 0
    if start == dest:
        return [0,1]
    
    N = len(Alist)  
    # Keep track of which nodes have already been visited
    D = {}
    for i in range(N):
        D[i] = 0
    D[start] = 1
    D1 = D.copy() # Keep track of which nodes have been visited updated after each iteration
    D2 = D.copy() # Keep track of number of shortest paths from start to each node
    sp = 0 # Keep track of length of shortest path
    
    Q = [start]
    
    while len(Q) > 0:
        # Update shortest path with each iteration
        sp +=1
        L = set([])
        for i in Q:
            for j in Alist[i]:
                # Check if node was visited in previous iterations
                if D1[j] == 0:
                    # Update number of shortest paths from start to node j
                    # that go through node i
                    D2[j] = D2[j] + D2[i]
                    L.add(j)
                    # Store node visits in D
                    D[j] = 1
        # If dest is visited this iteration, then break
        if D[dest] == 1: 
            Flights = [sp, D2[dest]]
            return Flights
        # Update D1
        D1 = D.copy()
        
        # Update Q
        Q = list(L)
    
    # Return empty list if loop ends without reaching dest
    return []



def safeJourney(Alist,start,dest):
    """
    Question 1.2 i)
    Find safest journey from station start to dest
    Input:
        Alist: List whose ith element contains list of 2-element tuples. The first element
        of the tuple is a station with a direct connection to station i, and the second element is
        the density for the connection.
    start, dest: starting and destination stations for journey.

    Output:
        Slist: Two element list containing safest journey and safety factor for safest journey
    """
    
    # If start and dest are the same, then there are no journeys
    if start == dest:
        return []
    
    # If start or dest are unconnected from the network, return []
    if Alist[start] == [] or Alist[dest] == []:
        return []
    
    
    N = len(Alist)
    
    # Compute dinit which is the maximum weight on an edge in Alist + 1
    dinit = 0
    for i in Alist:
        d = max(dict(i).values())
        if d > dinit:
            dinit = d 
    dinit += 1
    
    # Initialize dictionaries
    Edict = {} # Explored nodes
    Udict = {} # Unexplored nodes
    Pdict = {start:-1} # Keep track of paths, set start to -1, since we can't come 
    # from any node before start
    
    for n in range(N):
        Udict[n] = dinit
    Udict[start] = 0    
    
    # Initially set safety factor to 0 
    safety_factor = 0
    
    # Main search
    while len(Udict) > 0:   
        # Find node with min d in Udict and move to Edict
        dmin = dinit
        
        for n,w in Udict.items():
            if w < dmin:
                dmin = w
                nmin = n
        # If dmin is not updated then start and dest are not connected, so return empty list
        if dmin == dinit:
            return []
        Edict[nmin] = Udict.pop(nmin)
        
        # Required for keeping track of path
        prev = nmin
        
        # Update safety factor if we have to pass through an edge with a larger weight
        if dmin > safety_factor:
            safety_factor = dmin
        
        if dest in Edict:
            break

        # Update weights for unexplored neighbors of nmin
        for n in Alist[nmin]:

            if n[0] in Edict:
                pass
            elif n[0] in Udict:
                dcomp = n[1]
                if dcomp < Udict[n[0]]:
                    Udict[n[0]]= dcomp  
                    # Update path in Pdict
                    Pdict[n[0]] = prev
            else:
                dcomp = n[1]
                Udict[n[0]] = dcomp
    
    # Once at dest, can go back through Pdict and construct path from start to dest
    path = col.deque([dest])
    x = dest
    while x > start:
        path.appendleft(Pdict[x])
        x = Pdict[x]

    Slist = [list(path), safety_factor]

    return Slist



def shortJourney(Alist,start,dest):
    """
    Question 1.2 ii)
    Find shortest journey from station start to dest. If multiple shortest journeys
    exist, select journey which goes through the smallest number of stations.
    Input:
        Alist: List whose ith element contains list of 2-element tuples. The first element
        of the tuple is a station with a direct connection to station i, and the second element is
        the time for the connection (rounded to the nearest minute).
    start, dest: starting and destination stations for journey.

    Output:
        Slist: Two element list containing shortest journey and duration of shortest journey
    """
    
    # If start and dest are the same, then there are no journeys
    if start == dest:
        return []
    
    # If start or dest are unconnected from the network, return []
    if Alist[start] == [] or Alist[dest] == []:
        return []
    
    N = len(Alist)
    
    # Compute dinit which is the maximum weight on an edge in Alist + 1 multiplied by 
    # the number of nodes
    # This will be larger than all possible shortest paths between any two nodes in the network
    d = 0
    for i in Alist:
        d2 = max(dict(i).values())
        if d2 > d:
            d = d2
    dinit = (d + 1) * N
    
    # Initialize dictionaries
    Edict = {} # Explored nodes
    Udict = {} # Unexplored nodes
    Pdict = {start:-1} # Keep track of paths, set start to -1, since we can't come 
    # from any node before start
    Sdict = {start:0} # Keep track of steps 
    
    for n in range(N):
        Udict[n] = dinit
    Udict[start] = 0    
    
    # Main search
    while len(Udict) > 0:
        # Find node with min d in Udict and move to Edict
        dmin = dinit
        
        for n,w in Udict.items():
            if w < dmin:
                dmin = w
                nmin = n
        # If dmin is not updated then start and dest are not connected, so return empty list
        if dmin == dinit:
            return []
        Edict[nmin] = Udict.pop(nmin)
        
        # Required for keeping track of path
        prev = nmin
        
        if dest in Edict:
            break

        # Update provisional distances for unexplored neighbors of nmin
        for n in Alist[nmin]:

            if n[0] in Edict:
                pass
            elif n[0] in Udict:
                dcomp = dmin + n[1]
                if dcomp < Udict[n[0]]:
                    Udict[n[0]]= dcomp
                    # Update path in Pdict
                    Pdict[n[0]] = prev
                    # Update number of steps in Sdict
                    Sdict[n[0]] = Sdict[prev] + 1
                
                # If we have the same distance to n[0], but in less steps, 
                # then we update the path in Pdict and steps in Sdict
                if dcomp == Udict[n[0]] and Sdict[prev] < Sdict[Pdict[n[0]]]:
                    Pdict[n[0]] = prev
                    Sdict[n[0]] = Sdict[prev] + 1
            else:
                dcomp = dmin + n[1]
                Udict[n[0]] = dcomp 
    
    # Once at dest, can go back through Pdict and construct path from start to dest
    path = col.deque([dest])
    x = dest
    while x > start:
        path.appendleft(Pdict[x])
        x = Pdict[x]
        
    Slist = [list(path), Edict[dest]]
        
    return Slist, Edict


def cheapCycling(SList,CList):
    """
    Question 1.3
    Find first and last stations for cheapest cycling trip
    Input:
        Slist: list whose ith element contains cheapest fare for arrival at and
        return from the ith station (stored in a 2-element list or tuple)
        Clist: list whose ith element contains a list of stations which can be
        cycled to directly from station i
    Stations are numbered from 0 to N-1 with N = len(Slist) = len(Clist)
    Output:
        stations: two-element list containing first and last stations of journey
    """
    

    N = len(CList)
    
    # Keep track of remaining nodes that are not connected to the rest of the network using D and L1
    D = {}
    D1 = {} # Keep track of which nodes have not been visited
    for i in range(N):
        D[i] = i
        D1[i] = 0
    L1 = list(D.values()) 
    
    # Compute an upper bound for the total fare for arrival and return
    dinit = max(dict(SList))*2+1 
    
    # Iterate through each connected subnetwork, creating a list of nodes in the subnetwork,
    # a dictionary of the arrival cost at each of these node and a dictionary of return costs
    # from each of these nodes
    while len(L1) > 0:
        s = L1[0]
        Q = col.deque([s])
        D1[s] = 1
        L2 = []
        Adict = {}
        Rdict = {}
        
        # Slighly modified Breadth-first search to find connected subnetworks
        while len(Q)>0:
            x = Q.popleft()
            L2.append(D.pop(x))
            Adict[x] = SList[x][0]
            Rdict[x] = SList[x][1]
            for v in CList[x]:
                if D1[v] == 0:
                    Q.append(v)
                    D1[v] = 1
        L1 = list(D.values())
        
        # If the station is connected to at least one other station via cycling routes
        # then we can check possible pairs of stations
        if len(L2) != 1:
            # Compute minimum arrival and return fares and sum together
            arrivals = [Adict[i] for i in L2]
            returns = [Rdict[i] for i in L2]
            m = min(arrivals) + min(returns)
            # If this minimum is smaller than dinit, look for new minimum fare in 
            # this connected component
            if m < dinit:
                # Iterating through possible arrival and return total fares
                for i in L2:
                    for j in L2:
                        if i != j and Adict[i] + Rdict[j] < dinit:
                            # Update dinit and stations if we find a smaller total fare 
                            dinit = Adict[i] + Rdict[j]
                            stations = [i, j]
                            # If we find pair of stations whose fare is the same as this 
                            # minimum break
                            if dinit == m:
                                break
                    if dinit == m:
                        break

    return stations





if __name__=='__main__':
    #add code here if/as desired
    L=None #modify as needed
