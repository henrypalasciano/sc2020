""" Your college id here: 01333257
    Template code for part 1, contains 4 functions:
    newSort, merge: codes for part 1.1
    time_newSort: to be completed for part 1.1
    findTrough: to be completed for part 1.2
"""

import numpy as np
import time
import matplotlib.pyplot as plt


def newSort(X,k=0):
    """Given an unsorted list of integers, X,
        sort list and return sorted list
    """

    n = len(X)
    if n==1:
        return X
    elif n<=k:
        for i in range(n-1):
            ind_min = i
            for j in range(i+1,n):
                if X[j]<X[ind_min]:
                    ind_min = j
            X[i],X[ind_min] = X[ind_min],X[i]
        return X
    else:
        L = newSort(X[:n//2],k)
        R = newSort(X[n//2:],k)
        return merge(L,R)


def merge(L,R):
    """Merge 2 sorted lists provided as input
    into a single sorted list
    """
    M = [] #Merged list, initially empty
    indL,indR = 0,0 #start indices
    nL,nR = len(L),len(R)

    #Add one element to M per iteration until an entire sublist
    #has been added
    for i in range(nL+nR):
        if L[indL]<R[indR]:
            M.append(L[indL])
            indL = indL + 1
            if indL>=nL:
                M.extend(R[indR:])
                break
        else:
            M.append(R[indR])
            indR = indR + 1
            if indR>=nR:
                M.extend(L[indL:])
                break
    return M


def time_newSort(inputs=None):
    """Analyze performance of newSort
    Use variables inputs and outputs if/as needed
    """
    
    """Commenting and uncommenting parts of the code as 
    necessary"""
    
    #N = [100,500,1000,5000]
    N = [1000,5000,10000,50000]
    l=[0,0,0,1,1,0,1,1]
    #titles = ["N=100","N=500","N=1000","N=5000"]
    titles = ["N=1000","N=5000","N=10000","N=50000"]
    fig, axs = plt.subplots(2, 2)
    fig.tight_layout(pad=1)
    fig.set_size_inches(12, 8)
    for j,i in enumerate(N):
        L = list(np.random.randint(1,2*i,i))
        k = list(range(1,10,1))+list(range(10,105,5))#+list(range(100,i,i//10))
        times=[]
        for n in k:
            t1 = time.time()
            newSort(L,n)
            t2 = time.time()
            times.append(t2-t1)
        t3 = time.time()
        mergesort(L)
        t4=time.time()
        time2=[t4-t3]*21 #(i//50+1)
        x=list(range(0,105,5))
        axs[l[2*j], l[2*j+1]].plot(k,times)
        axs[l[2*j], l[2*j+1]].plot(x,time2,color='red')
        axs[l[2*j], l[2*j+1]].set_title(titles[j])
            
    for ax in axs.flat:
        ax.set(xlabel="k", ylabel="time")
        
        
    outputs=None
    return outputs


def findTrough(L):
    """Find and return a location of a trough in L
    """

    N = len(L)
    # Define index, which will be updated as the fuction runs
    index = 0
    # If L is empty cannot find trough
    if N == 0 or N==1:
        return -(N+1)
    # If not use TroughFinder (defined below) to find trough
    else:
        return TroughFinder (L, N, index)
        
            
def TroughFinder(L, N, M):
    
    m = N//2
    # If N = 1, have found trough
    if N == 1:
        return M
    # If N = 2, smallest element is trough
    if N == 2:
        if L[0] <= L[1]:
            return M
        else:
            return M+1
    # Otherwise check if middle value is a trough
    # If not, consider half list, updating M each time, using recursion
    # M keeps count of number of values discarded on left side of list
    else:
        # If condition is satisfied, have located trough
        if L[m-1] >= L[m] and L[m] <= L[m+1]:
            return M + m
        # If L[m-1] <= L[m], this means that there is a trough in left side of
        # list (see pdf)
        elif L[m-1] <= L[m]:
            return TroughFinder(L[:m], m, M)
        # Otherwise consider right side of list
        else:
            return TroughFinder(L[m+1:], N-(m+1), M+m+1)
    



if __name__=='__main__':
    inputs=None
    outputs=time_newSort(inputs)
