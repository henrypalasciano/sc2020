""" Your college id here: 01333257
    Template code for part 2, contains 3 functions:
    codonToAA: returns amino acid corresponding to input amino acid
    DNAtoAA: to be completed for part 2.1
    pairSearch: to be completed for part 2.2
"""


def codonToAA(codon):
	"""Return amino acid corresponding to input codon.
	Assumes valid codon has been provided as input
	"_" is returned for valid codons that do not
	correspond to amino acids.
	"""
	table = {
		'ATA':'i', 'ATC':'i', 'ATT':'i', 'ATG':'m',
		'ACA':'t', 'ACC':'t', 'ACG':'t', 'ACT':'t',
		'AAC':'n', 'AAT':'n', 'AAA':'k', 'AAG':'k',
		'AGC':'s', 'AGT':'s', 'AGA':'r', 'AGG':'r',
		'CTA':'l', 'CTC':'l', 'CTG':'l', 'CTT':'l',
		'CCA':'p', 'CCC':'p', 'CCG':'p', 'CCT':'p',
		'CAC':'h', 'CAT':'h', 'CAA':'q', 'CAG':'q',
		'CGA':'r', 'CGC':'r', 'CGG':'r', 'CGT':'r',
		'GTA':'v', 'GTC':'v', 'GTG':'v', 'GTT':'v',
		'GCA':'a', 'GCC':'a', 'GCG':'a', 'GCT':'a',
		'GAC':'d', 'GAT':'d', 'GAA':'e', 'GAG':'e',
		'GGA':'g', 'GGC':'g', 'GGG':'g', 'GGT':'g',
		'TCA':'s', 'TCC':'s', 'TCG':'s', 'TCT':'s',
		'TTC':'f', 'TTT':'f', 'TTA':'l', 'TTG':'l',
		'TAC':'y', 'TAT':'y', 'TAA':'_', 'TAG':'_',
		'TGC':'c', 'TGT':'c', 'TGA':'_', 'TGG':'w',
	}
	return table[codon]

def DNAtoAA(S):
    """Convert genetic sequence contained in input string, S,
    into string of amino acids corresponding to the distinct
    amino acids found in S and listed in the order that
    they appear in S
    """

    N = len(S)
    AA = ''   
    for i in range(N//3):
        # Use codonToAA to convert to amino acid        
        a = codonToAA(S[3*i:3*(i+1)])
        if a not in AA:
            AA +=a
        # Since there are only 21 distinct amino acids, can break when AA
        # reaches length 21
        if len(AA) == 21:
            break
    
    return  AA



def pairSearch(L,pairs):
    """Find locations within adjacent strings (contained in input list,L)
    that match k-mer pairs found in input list pairs. Each element of pairs
    is a 2-element tuple containing k-mer strings
    """
    
    # Choose prime q
    q=203
    # Compute N, which is the same for all DNA sequences
    N = len(L[0])
    # Compute k, which is the same for all k-mers, since k is constant
    k = len(pairs[0][0])
    bk = (4**k) % q
    locations = []
    hLlists = []
    # Iterate through each pair of adjacent sequences
    for i in range(len(L)):
        iL = char2base4(L[i])
        # Convert DNA sequences to lists of of base 10 integers modulo q
        hL = heval(iL[:k],4,q)
        hLlist = [hL]
        for j in range(1,N-k+1):
            hL = (4*hL - int(iL[j-1])*bk + int(iL[j-1+k])) % q
            hLlist.append(hL)
        hLlists.append(hLlist)
            
    # Iterate through each pair      
    for l in range(len(pairs)):
        ik1 = char2base4(pairs[l][0])
        ik2 = char2base4(pairs[l][1])
        # Convert the k-mer pair to base 10 integers modulo q
        hk1 = heval(ik1,4,q)
        hk2 = heval(ik2,4,q)
        
        for m in range(len(hLlists)-1):
            hL1 = hLlists[m]
            hL2 = hLlists[m+1]
            for ind in range(N-k+1):
                # If match in first list, check second list
                if hL1[ind] == hk1: 
                    if hL2[ind] == hk2: 
                        locations.append([ind,m,l])

    return locations


# From lectures have the following functions
def char2base4(S):
    """Convert gene test_sequence string to list of ints
    """
    c2b = {}
    c2b['A']=0
    c2b['C']=1
    c2b['G']=2
    c2b['T']=3
    L=[]
    for s in S:
        L.append(c2b[s])
    return L


def heval(L,Base,Prime):
    """Convert list L to base-10 number mod Prime where Base specifies the base of L
    """
    f=0
    for l in L[:-1]:
        f = Base*(l+f)
    h = (f + (L[-1])) % Prime
    return h
